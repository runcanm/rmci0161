package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class EmployeeMockTest {

    EmployeeRepositoryInterface repository;
    Employee employee;

    @Before
    public void setUp(){
        repository = new EmployeeMock();
    }

    @After
    public void tearDown(){
        repository = null;
    }

    @Test
    public void modifyEmployeeFunctionTest1() {
        employee = null;
        repository.modifyEmployeeFunction(employee,DidacticFunction.LECTURER);

        assertNull(employee);
    }

    @Test
    public void modifyEmployeeFunctionTest2() {
        employee = new Employee(1,"Marius", "Pacuraru", "1234567890876", DidacticFunction.ASISTENT, 2500d);
        repository.getEmployeeList().removeAll(repository.getEmployeeList());
        repository.modifyEmployeeFunction(employee,DidacticFunction.LECTURER);

        assertNull(repository.findEmployeeById(employee.getId()));
    }


    @Test
    public void modifyEmployeeFunctionTest3() {
        employee = new Employee(1,"Marius", "Pacuraru", "1234567890876", DidacticFunction.ASISTENT, 2500d);
        repository.modifyEmployeeFunction(employee,DidacticFunction.LECTURER);

        assertEquals(repository.findEmployeeById(employee.getId()).getFunction(),DidacticFunction.LECTURER);
    }

}