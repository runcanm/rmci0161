package AngajatiApp.repository;

import AngajatiApp.controller.DidacticFunction;
import AngajatiApp.model.Employee;
import AngajatiApp.validator.EmployeeException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class EmployeeImplTest {
    private EmployeeImpl repository;

    @Before
    public void setUp(){
        repository=new EmployeeImpl();
    }

    @After
    public void tearDown(){
        repository=null;
    }

    @Test
    public void TC1() throws EmployeeException {
        Employee employee  = new Employee("mihai","runcan","1111111111111", DidacticFunction.TEACHER,2000d);
        employee.setId(1);

        int employeeNumber=repository.getEmployeeList().size();
        repository.addEmployee(employee);
        assertEquals((employeeNumber+1),repository.getEmployeeList().size());
    }

    @Test (expected = Exception.class)
    public void TC2() throws EmployeeException {
        Employee employee  = new Employee("mihai","runcan","1111111111111", DidacticFunction.TEACHER,2000d);
        employee.setId(-1);

        int employeeNumber=repository.getEmployeeList().size();
        repository.addEmployee(employee);
        assertEquals(employeeNumber,repository.getEmployeeList().size());
    }

    @Test (expected = Exception.class)
    public void TC3() throws EmployeeException {
        Employee employee  = new Employee("mihai","runcan","1111111111111", DidacticFunction.TEACHER,-2d);
        employee.setId(-1);

        int employeeNumber=repository.getEmployeeList().size();
        repository.addEmployee(employee);
        assertEquals(employeeNumber,repository.getEmployeeList().size());
    }


}