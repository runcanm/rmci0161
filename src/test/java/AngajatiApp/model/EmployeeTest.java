package AngajatiApp.model;

import AngajatiApp.controller.EmployeeController;
import AngajatiApp.repository.EmployeeImpl;
import AngajatiApp.repository.EmployeeRepositoryInterface;
import org.junit.*;

import static AngajatiApp.controller.DidacticFunction.TEACHER;
import static org.junit.Assert.*;

public class EmployeeTest {
    private EmployeeController employeeController;
    private Employee employee;

    @BeforeClass
    public static void setUpAll(){
        System.out.println("Before all tests");
    }

    @AfterClass
    public static void tearDownAll(){
        System.out.println("After all tests");
    }

    @Before
    public void setUp(){
        System.out.println("Before test");
        EmployeeRepositoryInterface repository = new EmployeeImpl();
        employeeController = new EmployeeController(repository);
    }

    @After
    public void tearDown(){
        employeeController = null;
        System.out.println("After test");
    }


    @Test
    public void testGetName(){
        assertEquals("Mihai",employeeController.findEmployeeById(1).getFirstName());
    }

    @Test
    public void testSetSalary(){
        employeeController.findEmployeeById(3).setSalary(20.0);
        assertNotEquals("15.0", employeeController.findEmployeeById(3).getSalary().toString());
    }

    @Test
    public void testEmployeeConstructor(){
        employee = new Employee("Mihai","Runcan","1111111111111",TEACHER,1200.0);
        assertNotEquals(1000.0,employee.getSalary());
    }

    @Test
    public void testEmployeeById(){
        assertNull(employeeController.findEmployeeById(99999));
    }

    @Test
    public void testEmployeeEquals(){
        assertNotEquals(employeeController.findEmployeeById(1),employeeController.findEmployeeById(2));
    }

//    @Ignore
//    @Test
//    public void testEmployeeHashCode(){
//        assertNotEquals("",employeeController.findEmployeeById(5));
//    }


    @Test (timeout = 100 )
    public void testEmployeeToString(){
        assertNotNull(employeeController.findEmployeeById(4).toString());

    }
    @Test (expected = NullPointerException.class)
    public void testGetCNP(){
        assertEquals("exception not thrown on get CNP from null employee","1111111111111",employee.getCnp());
    }
}